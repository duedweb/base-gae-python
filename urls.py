# coding=utf-8
from webapp2_extras import jinja2
import webapp2
import config
import handlers


route = webapp2.WSGIApplication([
    webapp2.Route('/', handler=handlers.HomeHandler, name='home'),
    webapp2.Route('/page', handler=handlers.PaginaHandler, name='page'),
], debug=config.debug_mode, config=config.config)


def handle_404(request, response, exception):
    c = {'exception': exception.status}
    t = jinja2.get_jinja2(app=webapp2.WSGIApplication.active_instance).render_template('error404.html', **c)
    response.write(t)
    response.set_status(404)


def handle_500(request, response, exception):
    c = {'exception': exception}
    t = jinja2.get_jinja2(app=webapp2.WSGIApplication.active_instance).render_template('error500.html', **c)
    response.write(t)
    response.set_status(500)

route.error_handlers[404] = handle_404
# if not in debug mode show custom 500 error page
if not config.debug_mode:
    route.error_handlers[500] = handle_500