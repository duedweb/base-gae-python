# coding=utf-8
import webapp2
from webapp2_extras import jinja2, sessions


class BaseHandler(webapp2.RequestHandler):
    @webapp2.cached_property
    def jinja2(self):
        # Returns a Jinja2 renderer cached in the app registry.
        return jinja2.get_jinja2(app=self.app)

    def messages(self):
        return self.session.get_flashes()

    def render_response(self, _template, **context):
        # Renders a template and writes the result to the response.
        context.update({
            'messages': self.messages(),
        })
        rv = self.jinja2.render_template(_template, **context)
        self.response.write(rv)

    @webapp2.cached_property
    def session(self):
        """Shortcut to access the current session."""
        return self.session_store.get_session(backend='memcache')

    def display_message(self, message):
        """Utility function to display a template with a simple message."""
        self.session.add_flash(message)

    # this is needed for webapp2 sessions to work
    def dispatch(self):
        # Get a session store for this request.
        self.session_store = sessions.get_store(request=self.request)

        try:
            # Dispatch the request.
            webapp2.RequestHandler.dispatch(self)
        finally:
            # Save all sessions.
            self.session_store.save_sessions(self.response)


class HomeHandler(BaseHandler):
    def get(self):
        title = u'Title Home'
        keywords = u'home page, page 1'
        description = u'Page Description'  # max 150 char

        context = {
            'titolo': title,
            'keywords': keywords,
            'descrizione': description,
        }

        self.render_response('index.html', **context)


class PaginaHandler(BaseHandler):
    def get(self, *args, **kwargs):
        title = u'-- PAGE 2 --'
        keywords = u'page 2, p2'
        description = u'Page Description'  # max 150 char
        variable = kwargs['variable']

        context = {
            'titolo': title,
            'keywords': keywords,
            'descrizione': description,
        }

        self.render_response('page.html', **context)
