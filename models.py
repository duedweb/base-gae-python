from google.appengine.ext import ndb


class News(ndb.Model):
    date = ndb.DateTimeProperty(auto_now_add=True)
    title = ndb.StringProperty(required=True)
    text = ndb.TextProperty(required=True)