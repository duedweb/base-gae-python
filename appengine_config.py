import os
import sys

# Set path to import from the lib directory
sys.path.append(os.path.join(os.path.dirname(__file__), 'lib'))


# Middleware per AppStat
def webapp_add_wsgi_middleware(app):
    from google.appengine.ext.appstats import recording
    app = recording.appstats_wsgi_middleware(app)
    return app