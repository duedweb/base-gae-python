import webapp2


app_secret_key = 'place-here-a-long-and-difficult-key'

debug_mode = True
app_version = '1'


def key_obj(obj):
    return obj.key.urlsafe()

config = {
    'webapp2_extras.auth': {
        'user_model': 'models.User',
        'user_attributes': ['name'],
    },
    'webapp2_extras.sessions': {
        'secret_key': app_secret_key,
    },
    'webapp2_extras.jinja2': {
        'globals': {
            'url_for': webapp2.uri_for,
            'key': key_obj,
        }
    }
}