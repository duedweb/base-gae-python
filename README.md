# README #

This simple application is the base to start developing on GAE without accidentally forgetting something basic.

### What is this repository for? ###

* Rapid development to start immediately after download
* Version 1.0
* No Demo for now, sorry (is need?)

### How do I get set up? ###

* Download and open
* Edit file to match your project
* Import Dependencies in lib
* Write models configuration
* Run tests
* Deploy
* Enjoy

### Who do I talk to? ###

You can contact me by visiting my website ([www.2dweb.it](http://www.2dweb.it)) or write to paolo@*NOSPAM*2dweb.it (remove no spam tag)